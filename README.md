# dct

Fast, optimised O(nlgn) divide and conquer delaunay triangulation program.

See http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.56.2323 for details.

<meta name="google-site-verification" content="jxWwuS0slEsmFo5ne_gfdwpt-JcEfSlzdycBj2w-Kps" />