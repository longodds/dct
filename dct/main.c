
/*
 * Copyright (C) 2019, Geoff Leach <gl@rmit.edu.au>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.
 */
/*
 * Copyright (C) 2019, Geoff Leach <gl@rmit.edu.au>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.
 */
/*
 * Copyright (C) 2019, Geoff Leach <gl@rmit.edu.au>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include  "defs.h"
#include  "decl.h"
#include  "extern.h"
#include  "edge.h"
#include  <stdio.h>
#include  <stdlib.h>

int main(int argc, char *argv[])
{
  cardinal n;
  edge *l_cw, *r_ccw;
  index i;
  point **p_sorted, **p_temp;

  if (scanf("%d", &n) != 1)
    panic("Problem reading number of points on first line\n");

  if (n <= 0)
    panic("Number of points has to be greater than 0\n");

  alloc_memory(n);

  read_points(n);

  /* Initialise entry edge pointers. */
  for (i = 0; i < n; i++)
    p_array[i].entry_pt = NULL;

  /* Sort. */
  p_sorted = (point **)malloc((unsigned)n*sizeof(point *));
  if (p_sorted == NULL)
    panic("triangulate: not enough memory\n");
  p_temp = (point **)malloc((unsigned)n*sizeof(point *));
  if (p_temp == NULL)
    panic("triangulate: not enough memory\n");
  for (i = 0; i < n; i++)
    p_sorted[i] = p_array + i;

  merge_sort(p_sorted, p_temp, 0, n-1);
  
  free((char *)p_temp);


  /* Triangulate. */
  divide(p_sorted, 0, n-1, &l_cw, &r_ccw);

  free((char *)p_sorted);

  if (argc == 2)
    print_results(n, argv[1][1]);
  else
    print_results(n, 'e');

  free_memory();

  exit(EXIT_SUCCESS);
  return 0;	/* To keep lint quiet. */
}
